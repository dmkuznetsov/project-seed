module.exports = function (grunt) {
    require('load-grunt-tasks')(grunt);

    var getJsModules = function (basePath, patterns) {
        var result = [];
        grunt.log.debug('Search RequireJs modules in ' + basePath + '..');
        if (!grunt.util._.isArray(patterns)) {
            patterns = [patterns];
        }
        grunt.util._.each(patterns, function (pattern) {
            grunt.file.glob.sync(basePath + '/' + pattern).forEach(function (option) {
                option = option
                    .replace(basePath + '/', '')
                    .replace('.js', '');
                grunt.log.debug(option);
                result.push(option);
            });
        });
        return result;
    };

    grunt.initConfig({
        requirejs: {
            compile: {
                options: {
                    baseUrl: 'static/scripts',
                    waitSeconds: 30,
                    mainConfigFile: 'static/scripts/config.js',
                    name: '../vendor/almond/almond',
                    include: getJsModules('static/scripts', ['main.js', 'pages/*.js', 'pages/*/main.js']),
                    insertRequire: ['main'],
                    out: 'static/compiled.min.js',
                    wrap: {
                        start: '(function() {',
                        end: '})();'
                    }
                }
            }
        },

        less: {
            compile: {
                options: {
                    paths: ['static/styles'],
                    cleancss: false,
                    compress: false
                },
                files: {
                    'static/compiled.css': ['static/styles/*.less', 'static/styles/*/*.less']
                }
            }
        },

        concat: {
            options: {
                separator: '\n\n'
            },
            dist: {
                files: {
                    'static/compiled.css': ['static/compiled.css', 'static/styles/*.css', 'static/styles/*/*.css']
                }
            }
        },

        cssmin: {
            minify: {
                files: {
                    'static/compiled.min.css': ['static/compiled.css']
                }
            }
        },

        watch: {
            less: {
                files: ['static/styles/*.less', 'static/styles/*/*.less'],
                tasks: ['less', 'concat']
            },
            styles: {
                files: ['static/styles/*.css', 'static/styles/*/*.css'],
                tasks: ['concat']
            }
        }
    });

    grunt.registerTask('scripts', ['requirejs']);
    grunt.registerTask('styles', ['less', 'concat', 'cssmin']);
    grunt.registerTask('default', ['scripts', 'styles']);
};

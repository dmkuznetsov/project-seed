## Motivation

1. Quick start for development
2. Good architecture
3. Modern technologies


## What's inside

Build util [grunt](http://gruntjs.com/) with tasks for

1. Compile and minify [RequireJs](http://requirejs.org)
2. Compile and minify [less](http://lesscss.org/)
3. Minify css


## Requirements

1. [node.js](http://nodejs.org/)
2. [npm](https://www.npmjs.org/)


## Setup

```bash
# Clone requirejs-seed repo
git clone https://bitbucket.org/dmkuznetsov/project-seed.git project_dir
cd project_dir && rm -rf .git

# Install project dependencies (required)
bower install

# Install node.js modules for compile && minify
npm install
```


## Install JavaScript libraries

```bash
# Install Jquery and save it to bower.json
bower install jquery --save
```


## How to use

```html
<!-- For develop environment //-->
<link href="static/compiled.css" rel="stylesheet" type="text/css"/>
<script src="static/vendor/requirejs/require.js" data-main="static/scripts/config"></script>


<!-- For production environment //-->
<link href="static/compiled.min.css" rel="stylesheet" type="text/css"/>
<script src="static/compiled.min.js"></script>
```


## Compile && minify

```bash
# Compile and minify for production
grunt

# Watch for changes of styles in real-time
grunt watch

# Compile JavaScripts
grunt scripts

# Compile Less and Css
grunt styles
```

require.config({
    'baseUrl': 'static/scripts',
    'deps': ['main'],
    'paths': {
        'text': '../vendor/requirejs-text/text',
        'domReady': '../vendor/requirejs-domready/domReady'
    }
});

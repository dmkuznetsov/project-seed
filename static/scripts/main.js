define(['domReady'], function (domReady) {
    domReady(function () {
        var DATA_ATTR = 'data-page',
            DEFAULT_PAGE = 'main',
            $body = document.getElementsByTagName('body')[0],
            page = $body.getAttribute(DATA_ATTR);

        if (!page) {
            page = DEFAULT_PAGE;
            $body.setAttribute(DATA_ATTR, page);
        }

        require(['pages/' + page]);
    });
});
